#!/usr/bin/env python

from distutils.core import setup

setup(
    name='dslrentalsapi',
    version='0.1.0',
    description='Library to request and parse a dslrentals client\'s status',
    author='Chaps SD',
    author_email='drumchaps@gmail.com',
    url='https://bitbucket.org/drumchaps/dslrentalsapi',
    packages=["dslrentalsapi"],
    package_dir={"": "src"},
    #entry_points={
    #      'console_scripts': [
    #          'command_name = command_package.module:function',
    #  ],
    # },
    # package_data={'package': ['*.suffix']},
    # include_package_data=True,
    install_requires=[
        "requests",
        "bs4",
    ],
    # dependency_links=['', ]
)
