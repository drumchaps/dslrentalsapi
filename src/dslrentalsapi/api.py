import requests
from bs4 import BeautifulSoup


class DSLRentals(object):

    homeurl = "http://dslrentals.somee.com/api/myproxy.asp"

    def __init__(self, key=None):
        self.key = key
        pass

    def change_ip(self):
        params = {}
        params["key"] = self.key
        data = {}
        data["cmd"] = 25
        r = requests.post(self.homeurl, params=params, data=data)
        self.r = r
        soup = BeautifulSoup(r.text)
        tds = soup.find("table").findAll("tr")[1].findAll("td")
        return tds[0].text, tds[1].text, tds[2].text, tds[3].text

        return soup

    def get_ip(self):
        data = {}
        data["key"] = self.key
        r = requests.get(self.homeurl, params=data)
        self.r = r
        soup = BeautifulSoup(r.text)
        tds = soup.find("table").findAll("tr")[1].findAll("td")
        return tds[0].text, tds[1].text, tds[2].text, tds[3].text

    pass
